package io.enoa.toolkit.ret;

import io.enoa.toolkit.sys.ThrowableKit;

public class Resp<T> implements Ret {

  private int err;
  private String code;
  private String msg;
  private T data;
  private String fullmsg;
  private String trace;

  public Resp() {
  }


  public static <T> Resp<T> with(boolean ok) {
    return with(ok, null);
  }

  public static <T> Resp<T> with(boolean ok, String msg) {
    Resp<T> resp = new Resp<>();
    resp.setMsg(msg);
    resp.setErr(ok ? 0 : 1);
    return resp;
  }

  public static <T> Resp<T> ok() {
    return Resp.with(true);
  }

  public static <T> Resp<T> ok(T data) {
    return Resp.<T>with(true).data(data);
  }

  public static <T> Resp<T> err(Throwable throwable) {
    return err(null, throwable);
  }

  public static <T> Resp<T> err(String msg) {
    return err(msg, null);
  }

  public static <T> Resp<T> err(String msg, Throwable throwable) {
    return Resp.<T>with(false)
      .msg(msg)
      .throwable(throwable);
  }


  public int getErr() {
    return err;
  }

  public void setErr(int err) {
    this.err = err;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  public T getData() {
    return data;
  }

  public void setData(T data) {
    this.data = data;
  }

  public String getFullmsg() {
    return fullmsg;
  }

  public void setFullmsg(String fullmsg) {
    this.fullmsg = fullmsg;
  }

  public String getTrace() {
    return trace;
  }

  public void setTrace(String trace) {
    this.trace = trace;
  }

  public Resp<T> data(T data) {
    this.setData(data);
    return this;
  }

  public Resp<T> msg(String msg) {
    this.setMsg(msg);
    return this;
  }

  public Resp<T> msgerr(String msg) {
    if (this.err == 1) {
      this.setMsg(msg);
    }
    return this;
  }

  public Resp<T> fullmsg(String fullmsg) {
    this.setFullmsg(fullmsg);
    return this;
  }

  public Resp<T> throwable(Throwable throwable) {
    this.setTrace(ThrowableKit.string(throwable));
    return this;
  }

  public Resp<T> code(String code) {
    this.setCode(code);
    return this;
  }

  public Resp<T> code(IRespCode code) {
    this.setCode(code == null ? null : code.name());
    return this;
  }
}
