package io.enoa.toolkit.ret;

import io.enoa.toolkit.is.Is;
import io.enoa.toolkit.map.EoMap;
import io.enoa.toolkit.map.Kv;
import io.enoa.toolkit.sys.ThrowableKit;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class Respm extends HashMap<String, Object> implements EoMap<Respm>, Ret {

  private Kv kv;

  public Respm() {
    this.kv = Kv.create();
  }

  public static Respm with(boolean ok) {
    return with(ok, null);
  }

  public static Respm with(boolean ok, String msg) {
    return ok ?
      new Respm().set("err", 0).set("msg", msg) :
      new Respm().set("err", 1).set("msg", msg);
  }

  public static Respm ok() {
    return Respm.with(true);
  }

  public static Respm ok(Object data) {
    return Respm.with(true).data(data);
  }

  public static Respm err(Throwable throwable) {
    return err(null, throwable);
  }

  public static Respm err(String msg) {
    return err(msg, null);
  }

  public static Respm err(String msg, Throwable throwable) {
    return Respm.with(false)
      .msg(msg)
      .throwable(throwable);
  }

  public Respm code(IRespCode code) {
    return this.code(code == null ? null : code.name());
  }

  public Respm code(String code) {
    if (Is.not().truthy(code)) {
      return this;
    }
    this.set("code", code);
    return this;
  }

  public Respm throwable(Throwable throwable) {
    if (Is.not().nullx(throwable)) {
      this.set("trace", ThrowableKit.string(throwable));
    }
    return this;
  }

  public Respm msg(String msg) {
    this.set("msg", msg);
    return this;
  }

  public Respm msgerr(String msg) {
    if (this.integer("err") == 1)
      this.set("msg", msg);
    return this;
  }

  public Respm data(Object data) {
    if (data == null) {
      return this;
    }
    if (data instanceof Optional<?>) {
      if (!((Optional<?>) data).isPresent()) {
        return this;
      }
    }
    this.set("data", data);
    return this;
  }

  public Respm fullmsg(String msg) {
    this.set("fullmsg", msg);
    return this;
  }

  @Override
  public Map<String, Object> map() {
    return this.kv;
  }

}
